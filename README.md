# decode_saml

Decode SAML assertions

    $ decode_saml.pl 
    Usage: decode_saml.pl [OPTIONS] [SAML ASSERTION(s)]
    
    Decode SAML assertions from files or STDIN.
        -c, --escape        Clean up C escape sequences (e.g. \n, \t etc.)
        -f, --file          Read from file
        -g, --get           URL decode too (for query-strings)
        -v, --version       Print version and exit
        -z, --zip           Inflate compression

This script relies on Encode, URI::Escape, MIME::Base64 and
Compress::Zlib at minimum

## Examples

- Decode SAML request from query-string (use `-g` for URL decoding and
  `-z` for Inflate decompression):

```
$ decode_saml.pl -z -g 'fZLLbsIwEEV%2FJfI%2BMUkTKBYgpdAHEoUI0i66qYwzgCXHph6nj7%2BvCa1KVZXtzNzxPXc8QF6rPcsbt9NLeGkAXfBeK42sbQxJYzUzHCUyzWtA5gRb5fczlkQdtrfGGWEUOZGcV3BEsE4aTYLpZEgW8%2BvZ4nY6f%2BYiy0SSirB7mfIwzXgc9pOsFybdHlzGkIq0L0jwCBa9dkj8Kr8AsYGpRse186VO0gnjOEwuyrjPOj2WdZ9IMPE8UnPXqnbO7ZFRuoEKbFuLnO9HQkDEN1EtFd0boyiioQcUyn0qoJ0U3AEJii%2FaK6krqbfnQdfHIWR3ZVmExWJVkiD%2Fhh8bjU0NdgX2VQp4WM5%2BzK05wh9bByNUma3U0c7VZDQ42GNtAHZ0Tjmgp5OD47Hn3u10UhglxUdwY2zN3f8wcRS3FVmFm3aUQc2lyqvKAqKHUsq8jS34iIbE2cYHRUfHZ39%2Fq9En'
```

- Decode SAML response from POST body (use `-f` for file as the POST
  body can be too large/long):

```
$ decode_saml.pl -f samlResponse.txt
```

- You can specify -f multiple times for multiple files

```
$ decode_saml.pl -f samlRequest.txt -f samlResponse.txt
```

- Most machines have a tool called `xmllint` installed; this can be
  used to pretty up ugly/unformatted XML output:

```
$ decode_saml.pl -f samlResponse.txt | xmllint --format -
```
