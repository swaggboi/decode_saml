#!/usr/bin/env perl

# Daniel Bowling <swaggboi@slackware.uk>
# Nov 2020

use strict;
use warnings;
use utf8;
use open qw{:std :utf8};
use File::Basename;
use Getopt::Long qw{:config no_ignore_case};
use Encode;
use URI::Escape;
use MIME::Base64;
use Compress::Zlib;
#use Data::Dumper; # Uncomment for debugging

my $version = '0.5';
my $cmd     = basename($0);
my ($i, @input, $samlReqOut, $status);

GetOptions(
    'escape|c'  => \my $escape,
    'file|f=s'  => \my @files,
    'get|g'     => \my $get,
    'version|v' => \my $ver,
    'zip|z'     => \my $zip
    );

# Choose what to do given args, STDIN, or null 
if (@ARGV) {
    @input = @ARGV          # Use args or...
}
elsif (! -t STDIN) {
    chomp(@input = <STDIN>) # Use STDIN or...
}
# Read from file(s) if -f/--file specified
elsif ($files[0]) {
    # Local vars for this bit
    my ($fh, $file, $line);
    
    # Grab the lines from each file
    for $file (@files) {
        open($fh, '<', $file) || die $!;
        push(@input, $line) while $line = <$fh>;
    }
}
# Print version and exit if -v/--version
elsif ($ver) {
    print "$version\n";
    exit(64);
}
# Print usage stuff if no input found
else {
    die <<~"OPTIONS";
        Usage: $cmd [OPTIONS] [SAML ASSERTION(s)]
        
        Decode SAML assertions from files or STDIN.
            -c, --escape        Clean up C escape sequences (e.g. \\n, \\t etc.)
            -f, --file          Read from file
            -g, --get           URL decode too (for query-strings)
            -v, --version       Print version and exit
            -z, --zip           Inflate compression
        OPTIONS
}

# Begin main loop
for my $samlReqIn (@input) {
    # Eliminate escape sequences if -c/--escape
    $samlReqIn =~ s/\\[abefnrtv\\'"?]//g if $escape;

    # URL decode if -g/--get
    $samlReqIn = Encode::decode 'utf8', uri_unescape $samlReqIn if $get;

    # Base64 decode
    $samlReqIn = decode_base64($samlReqIn);

    # Inflate (must use WindowBits => -MAX_WBITS for RFC 1951
    # compliance) if -z/--zip
    if ($zip) {
        ($i,          $status) = inflateInit(-WindowBits => -&MAX_WBITS);
        ($samlReqOut, $status) = $i->inflate($samlReqIn);
    }
    # Or not
    else {
        $samlReqOut = $samlReqIn
    }

    # Send it if $status says nothing bad happened OR if we're not
    # using -z/--zip
    if (!$zip || $status eq 'stream end') {
        print "$samlReqOut\n"
    }
    # If we're using -z/--zip and the status is bad, throw warning
    else {
        warn "$cmd: Inflate error - $status\n"
    }
}
